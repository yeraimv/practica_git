# practica_git

## Creación del repositorio en GitLab
He creado un repositorio público con un README.md en GitLab

![repo_creado_gitlab](screenshots/repo_creado_gitlab.png)

## Clonación del repositorio de GitLab
He clonado el repositorio en el escritorio de mi PC

![repo_clonado_gitlab](screenshots/repo_clonado_gitlab.png)

# IMPORTANTE
La documentación del proceso repetido en GitHub estará en el [README.md del propio GitHub](https://github.com/yeraimv/practica_git/blob/main/README.md)
